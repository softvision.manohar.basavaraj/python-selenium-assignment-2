from selenium import webdriver
from lxml import etree
import urllib.request
from selenium.webdriver.common.action_chains import ActionChains
import re
import logging
import logging.config

logging.config.fileConfig('logging.conf')
# create logger
logger = logging.getLogger('Python Selenium- Assignment 2')
logging.basicConfig(level=logging.INFO)

# Provide Driver Path
driver = webdriver.Chrome('C:/Users/ADMIN/Documents/chrome/chromedriver.exe')

# Maximize the Winow
driver.maximize_window()

# Launch the Webpage
driver.get("https://www.rahulshettyacademy.com/AutomationPractice/ ")

# Identify the required RadioButton
radioButtonTwo = driver.find_element_by_xpath("//input[@value='radio2']")

# Select the RadioButton
radioButtonTwo.click()

# Assert Radio Button
if radioButtonTwo.is_selected():
    assert True, 'Radio Button 2 - Not selected'

# Identify and select the country Indonesia
suggestionDropDown = driver.find_element_by_xpath("//input[@id='autocomplete']")
suggestionDropDown.send_keys("Indonesia")
driver.implicitly_wait(3)

# Assert Indonesia
if suggestionDropDown.text == "Indonesia":
    assert True, 'Selected suggestion is not Indonesia'

# Select Option two from the dropdown
optionTwo = driver.find_element_by_xpath("//select[contains(@id,'dropdown-class-example')]//option[@value='option2']")
optionTwo.click()

# Assert Option 2
if optionTwo.text == 'option2':
    assert True

# Interact with Alert component
driver.find_element_by_xpath("//input[contains(@name,'name')]").send_keys("Manohar")
driver.find_element_by_xpath("//input[contains(@id,'alertbtn')]").click()

# Switch to Alert box
nameAlertHandle = driver.switch_to.alert
checkNameFromAlert = nameAlertHandle.text
logger.info('This is message from alert -  %s', checkNameFromAlert)

# Fetching the name from the Alert message and Assert
myName = checkNameFromAlert.split(" ")
#logger.info(myName)
if myName[1] == "Manohar,":
    nameAlertHandle.accept()
    assert True

# Marking Parent window and #Switching to child window
parentWindow = driver.window_handles[0]
driver.find_element_by_xpath("//button[contains(@id,'openwindow')]").click()
childWindow = driver.window_handles[1]
driver.switch_to.window(childWindow)

# Fetching the Window Title, Assert and close the child window
logger.info('This is the title of new window -  %s', driver.title)
if driver.title == "QA Click Academy | Selenium,Jmeter,SoapUI,Appium,Database testing,QA Training Academy":
    assert True
    driver.close()

# Switching back to parent window
driver.switch_to.window(parentWindow)

# Reading Table data using lxml library
web = urllib.request.urlopen("https://www.rahulshettyacademy.com/AutomationPractice/")
s = web.read()
html = etree.HTML(s)

# Get all 'tr'
tr_nodes = html.xpath("//table[contains(@id,'product')]//tr")

# 'th' is inside first 'tr'
# header = [i[0].text for i in tr_nodes[0].xpath("th")]

# Get text from rest all 'tr'
td_content = [[td.text for td in tr.xpath('td[2]')] for tr in tr_nodes[1:]]

# Counter to set the count for Selenium courses
countOfSeleniumCourses = 0
for gotData in td_content:
    for data in gotData:
        seleniumHunt = data.split(" ")
        if "Selenium" in seleniumHunt:
            # logger.info('Found it')
            countOfSeleniumCourses += 1
logger.info('The Count of Selenium courses is -  %s', countOfSeleniumCourses)

# Working with Hide Button
driver.find_element_by_xpath("//input[@id='hide-textbox']").click()
textbox = driver.find_element_by_xpath("//input[@id='displayed-text']")

if textbox.is_displayed():
    assert True, 'The text box is still visible'

# Work with Mouseover
action = ActionChains(driver)
mouseoverButton = driver.find_element_by_xpath("//button[@id='mousehover']")
action.move_to_element(mouseoverButton).perform()
mouseoverData = []
for options in driver.find_elements_by_xpath("//div[@class='mouse-hover-content']//a"):
    title = options.text
    mouseoverData.append(title)
logger.info('The Mouse over data -  %s', mouseoverData)

# Working with iFrames
frame = driver.find_elements_by_tag_name('iframe')
totalFrames = len(frame)
logger.info('Total number of iFrames are -  %s', totalFrames)

# Switch to first frame
firstFrame = frame[0]
driver.switch_to.frame(firstFrame)
logger.info('Now you are inside the frame')
driver.switch_to.default_content()
logger.info('You are back in the main window')

# Close the browser
driver.quit()
logger.info('Browser Closed')
